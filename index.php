<?php

require 'vendor/autoload.php';
include 'bootstrap.php';

use Chatter\Models\Message;
use Chatter\Middleware\Logging as ChatterLogging;
use Chatter\Middleware\Authentication as ChatterAuth;
use Chatter\Middleware\ImageRemoveExif;
use Chatter\Middleware\FileFilter;

$app = new \Slim\App();
$app->add(new ChatterAuth());
$app->add(new ChatterLogging());

$app->get('/hello/{name}' , function($request, $response, $args){
    return $response->write("Hello " . $args['name']);
});

//ruta get: trae todos los mensajes
$app->get('/messages' , function($request, $response, $args){
    $_message = new Message();
    $messages = $_message->all();
    $payload = [];
    foreach($messages as $_msg){
        $payload[$_msg->id] = [
                                'body'=>$_msg->body,
                                'user_id' => $_msg->user_id,
                                'user_uri' => '/user/' . $_msg->user_id,
                                'created_at' => $_msg->created_at,
                                'image_url' => $_msg->image_url,
                                'message_id' => $_msg->id,
                                'message_uri' => '/messages/'
                              ];
    }

    return $response->withStatus(200)->withJson($payload);
    //var_dump($payload);
});

$filter = new FileFilter();
$removeExif = new ImageRemoveExif();

//ruta post: Crea un nuevo mensaje
$app->post('/messages' , function($request , $response, $args){
   $_message = $request->getParsedBodyParam('message' , '');

   $imagepath = '';
   $files = $request->getUploadedFiles();
   $newfile = $files['file'];
   if($newfile->getError()=== UPLOAD_ERR_OK){
       $uploadFileName = $newfile->getClientFileName();
       $newfile->moveTo("assets/images/" . $uploadFileName);
       $imagepath = "assets/images/"  . $uploadFileName;
   }

   $message = new Message();
   $message->body = $_message;
   $message->user_id = -1;
   $message->image_url = $imagepath;
   $message->save();
   if($message->id){
       $payload = [
           'message_id' => $message->id,
           'message_uri' => '/messages/' . $message->id,
           'image_url' => $message->image_url
       ];
       return $response->withStatus(201)->withJson($payload);
   }else{
       return $response->withStatus(400);
   }

})->add($filter);

$app->delete('/messages/{message_id}' , function($request , $response, $args){
    $message = Message::find($args['message_id']);
    $message->delete();
    if($message->exists){
        return $response->withStatus(400);
    }else{
        return $response->withStatus(204);
    }
});


$app->run();